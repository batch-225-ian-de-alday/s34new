const express = require("express");
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

/* 
1. Create a GET route that will access the "/home" route that will print out a simple message.
2. Process a GET request at the "/home" route using postman.
*/
app.get("/home", (req,res) => {
    res.send("Welcome to the homepage!")
});


/* 
3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
4. Process a GET request at the "/users" route using postman.
*/

let users = [
	{
		"userName" : "luffy",
		"password" : "1234"
	},
    {
		"userName" : "zoro",
		"password" : "12345"
	},
	{
		"userName" : "sanji",
		"password" : "123456"
	},
	{
		"userName" : "yamato",
		"password" : "1234567"
	}
];


app.get("/users", (req,res) => {
    res.send(users)
});

/* 
5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
6. Process a DELETE request at the "/delete-user" route using postman.
*/

app.delete("/delete-user", (req, res) => {

    console.log(req.body);
    
  
        existingUser = users.find(user => user.userName === req.body.userName) //this will loop on the users database and will find if there username = req.body.userName will return the object found
        isUserExists = Boolean(existingUser) // this will convert the object to a boolean this will return true
        isCorrectPassword = isUserExists && existingUser.password === req.body.password // this will check if existing password of the user will match to the req.body.password and return true
        isCorrectCredential = isUserExists && isCorrectPassword //this will check if the input user name and password input and is true to database username and password and will return true

        if(isCorrectCredential) {

            // this will filter the users data base and will return all the username that is not equal to req.body.userName and this code is to verify if the user has been deleted already.
            users = users.filter(user => {return user.userName != req.body.userName })
            
            // alternative solution using findIndex and splice 
            /* let index = users.findIndex(user => user.userName === req.body.userName);
            users.splice(index, 1); */
            
            // This will response in the Postman/Cient
            res.send(`User ${req.body.userName} has beed deleted successfully!`)
        } else {

            // if the condition isCorrectCredential does not meet this error message will be sent back to the client/ postman
            res.send("Please input correct username and password to delete user account")
        }
    // }
});


app.listen(port, () => console.log(`Server is running at port ${port}`))